﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;


namespace Signal
{
    //Работа с параметрами
    static class Settings
    {
        public static void SaveParameters(int timePeriod, int validStopTime, int validShiftChangeTimeByTheStartOfTheShift, int validShiftChangeTimeByTheEndOfTheShift, int validLoadingTime, int validNumberOfStops)
        {
            if (timePeriod > 0)
            {
                XElement settings = new XElement("settings");
                XDocument xdoc = new XDocument(settings);

                settings.Add(new XElement("timePeriod", timePeriod));
                settings.Add(new XElement("validStopTime", validStopTime));
                settings.Add(new XElement("validShiftChangeTimeByTheStartOfTheShift", validShiftChangeTimeByTheStartOfTheShift));
                settings.Add(new XElement("validShiftChangeTimeByTheEndOfTheShift", validShiftChangeTimeByTheEndOfTheShift));
                settings.Add(new XElement("validLoadingTime", validLoadingTime));
                settings.Add(new XElement("validNumberOfStops", validNumberOfStops));
                xdoc.Save("parameters.xml");
            }
            else
            {
                timePeriod = 1000;
                SaveParameters(timePeriod, validStopTime, validShiftChangeTimeByTheStartOfTheShift, validShiftChangeTimeByTheEndOfTheShift, validLoadingTime, validNumberOfStops);
            }
        }
        public static int LoadTimePeriod ()
        {
            int timePeriod = 1000;
            XDocument currentSettings = XDocument.Load("parameters.xml");
            foreach (XElement el in currentSettings.Root.Elements())
            {
                if (Convert.ToString(el.Name) == "timePeriod" && Convert.ToInt32(el.Value) > 0)
                {
                    timePeriod = Convert.ToInt32(el.Value);
                }
            }
            return timePeriod;
        }
        public static int LoadValidStopTime()
        {
            int validStopTime = 0;
            XDocument currentSettings = XDocument.Load("parameters.xml");
            foreach (XElement el in currentSettings.Root.Elements())
            {
                if (Convert.ToString(el.Name) == "validStopTime")
                {
                    validStopTime = Convert.ToInt32(el.Value);
                }
            }
            return validStopTime;
        }
        public static int LoadValidShiftChangeTimeByTheStartOfTheShift()
        {
            int validShiftChangeTimeByTheStartOfTheShift = 0;
            XDocument currentSettings = XDocument.Load("parameters.xml");
            foreach (XElement el in currentSettings.Root.Elements())
            {
                if (Convert.ToString(el.Name) == "validShiftChangeTimeByTheStartOfTheShift")
                {
                    validShiftChangeTimeByTheStartOfTheShift = Convert.ToInt32(el.Value);
                }
            }
            return validShiftChangeTimeByTheStartOfTheShift;
        }
        public static int LoadValidShiftChangeTimeByTheEndOfTheShift()
        {
            int validShiftChangeTimeByTheEndOfTheShift = 0;
            XDocument currentSettings = XDocument.Load("parameters.xml");
            foreach (XElement el in currentSettings.Root.Elements())
            {
                if (Convert.ToString(el.Name) == "validShiftChangeTimeByTheEndOfTheShift")
                {
                    validShiftChangeTimeByTheEndOfTheShift = Convert.ToInt32(el.Value);
                }
            }
            return validShiftChangeTimeByTheEndOfTheShift;
        }
        public static int LoadValidLoadingTime()
        {
            int validLoadingTime = 0;
            XDocument currentSettings = XDocument.Load("parameters.xml");
            foreach (XElement el in currentSettings.Root.Elements())
            {
                if (Convert.ToString(el.Name) == "validLoadingTime")
                {
                    validLoadingTime = Convert.ToInt32(el.Value);
                }
            }
            return validLoadingTime;
        }
        public static int LoadValidNumberOfStops()
        {
            int validNumberOfStops = 0;
            XDocument currentSettings = XDocument.Load("parameters.xml");
            foreach (XElement el in currentSettings.Root.Elements())
            {
                if (Convert.ToString(el.Name) == "validNumberOfStops")
                {
                    validNumberOfStops = Convert.ToInt32(el.Value);
                }
            }
            return validNumberOfStops;
        }
        public static string LoadConnectionString() //загрузка строки соединения из XML
        {
            try
            {
                XDocument currentSettings = XDocument.Load("databaseConnectionString.xml");
                foreach (XElement el in currentSettings.Root.Elements())
                {
                    if (Convert.ToString(el.Name)=="connectionString")
                    {
                        return Convert.ToString(el.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "ERROR";
        }
        public static bool CheckDatabaseConnection () //проверка соединения с БД
        {
            using (SqlConnection connection = new SqlConnection(LoadConnectionString()))
            {
                try
                {
                    connection.Open();
                    try
                    {
                        SqlCommand cmd = new SqlCommand("SELECT 1", connection);
                        SqlDataReader reader = cmd.ExecuteReader();
                    }    
                    catch (Exception ex)
                    {
                        return false;
                    }              
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }
        public static void SaveCSVReport(string CSVFileName, string CSVContent) //сохранение csv
        {
            File.WriteAllText(CSVFileName+".csv", CSVContent, Encoding.UTF8);
        }
    }
}
