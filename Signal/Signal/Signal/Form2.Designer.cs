﻿namespace Signal
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTimePeriod = new System.Windows.Forms.Label();
            this.numericUpDownTimePeriod = new System.Windows.Forms.NumericUpDown();
            this.buttonSave = new System.Windows.Forms.Button();
            this.numericUpDownValidStopTime = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownValidShiftChangeTimeByTheStartOfTheShift = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownValidLoadingTime = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownValidNumberOfStops = new System.Windows.Forms.NumericUpDown();
            this.labelValidStopTime = new System.Windows.Forms.Label();
            this.labelValidShiftChangeTimeByTheStartOfTheShift = new System.Windows.Forms.Label();
            this.labelLoadValidShiftChangeTimeByTheEndOfTheShift = new System.Windows.Forms.Label();
            this.labelValidLoadingTime = new System.Windows.Forms.Label();
            this.labelValidNumberOfStops = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimePeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValidStopTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValidShiftChangeTimeByTheStartOfTheShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValidLoadingTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValidNumberOfStops)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTimePeriod
            // 
            this.labelTimePeriod.AutoSize = true;
            this.labelTimePeriod.Location = new System.Drawing.Point(353, 18);
            this.labelTimePeriod.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelTimePeriod.Name = "labelTimePeriod";
            this.labelTimePeriod.Size = new System.Drawing.Size(339, 32);
            this.labelTimePeriod.TabIndex = 0;
            this.labelTimePeriod.Text = "Период обновления, сек";
            // 
            // numericUpDownTimePeriod
            // 
            this.numericUpDownTimePeriod.Location = new System.Drawing.Point(17, 16);
            this.numericUpDownTimePeriod.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.numericUpDownTimePeriod.Name = "numericUpDownTimePeriod";
            this.numericUpDownTimePeriod.Size = new System.Drawing.Size(320, 38);
            this.numericUpDownTimePeriod.TabIndex = 1;
            // 
            // buttonSave
            // 
            this.buttonSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonSave.Location = new System.Drawing.Point(17, 328);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(320, 60);
            this.buttonSave.TabIndex = 2;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            // 
            // numericUpDownValidStopTime
            // 
            this.numericUpDownValidStopTime.Location = new System.Drawing.Point(17, 68);
            this.numericUpDownValidStopTime.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.numericUpDownValidStopTime.Name = "numericUpDownValidStopTime";
            this.numericUpDownValidStopTime.Size = new System.Drawing.Size(320, 38);
            this.numericUpDownValidStopTime.TabIndex = 3;
            // 
            // numericUpDownValidShiftChangeTimeByTheStartOfTheShift
            // 
            this.numericUpDownValidShiftChangeTimeByTheStartOfTheShift.Location = new System.Drawing.Point(17, 120);
            this.numericUpDownValidShiftChangeTimeByTheStartOfTheShift.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.numericUpDownValidShiftChangeTimeByTheStartOfTheShift.Name = "numericUpDownValidShiftChangeTimeByTheStartOfTheShift";
            this.numericUpDownValidShiftChangeTimeByTheStartOfTheShift.Size = new System.Drawing.Size(320, 38);
            this.numericUpDownValidShiftChangeTimeByTheStartOfTheShift.TabIndex = 4;
            // 
            // numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift
            // 
            this.numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift.Location = new System.Drawing.Point(17, 172);
            this.numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift.Name = "numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift";
            this.numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift.Size = new System.Drawing.Size(320, 38);
            this.numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift.TabIndex = 5;
            // 
            // numericUpDownValidLoadingTime
            // 
            this.numericUpDownValidLoadingTime.Location = new System.Drawing.Point(17, 224);
            this.numericUpDownValidLoadingTime.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.numericUpDownValidLoadingTime.Name = "numericUpDownValidLoadingTime";
            this.numericUpDownValidLoadingTime.Size = new System.Drawing.Size(320, 38);
            this.numericUpDownValidLoadingTime.TabIndex = 6;
            // 
            // numericUpDownValidNumberOfStops
            // 
            this.numericUpDownValidNumberOfStops.Location = new System.Drawing.Point(17, 276);
            this.numericUpDownValidNumberOfStops.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.numericUpDownValidNumberOfStops.Name = "numericUpDownValidNumberOfStops";
            this.numericUpDownValidNumberOfStops.Size = new System.Drawing.Size(320, 38);
            this.numericUpDownValidNumberOfStops.TabIndex = 8;
            // 
            // labelValidStopTime
            // 
            this.labelValidStopTime.AutoSize = true;
            this.labelValidStopTime.Location = new System.Drawing.Point(353, 70);
            this.labelValidStopTime.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelValidStopTime.Name = "labelValidStopTime";
            this.labelValidStopTime.Size = new System.Drawing.Size(463, 32);
            this.labelValidStopTime.TabIndex = 10;
            this.labelValidStopTime.Text = "Допутимое время остановки, мин";
            // 
            // labelValidShiftChangeTimeByTheStartOfTheShift
            // 
            this.labelValidShiftChangeTimeByTheStartOfTheShift.AutoSize = true;
            this.labelValidShiftChangeTimeByTheStartOfTheShift.Location = new System.Drawing.Point(353, 122);
            this.labelValidShiftChangeTimeByTheStartOfTheShift.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.labelValidShiftChangeTimeByTheStartOfTheShift.Name = "labelValidShiftChangeTimeByTheStartOfTheShift";
            this.labelValidShiftChangeTimeByTheStartOfTheShift.Size = new System.Drawing.Size(850, 32);
            this.labelValidShiftChangeTimeByTheStartOfTheShift.TabIndex = 11;
            this.labelValidShiftChangeTimeByTheStartOfTheShift.Text = "Допустимая длительность пересменки при начале смены, мин";
            // 
            // labelLoadValidShiftChangeTimeByTheEndOfTheShift
            // 
            this.labelLoadValidShiftChangeTimeByTheEndOfTheShift.AutoSize = true;
            this.labelLoadValidShiftChangeTimeByTheEndOfTheShift.Location = new System.Drawing.Point(353, 174);
            this.labelLoadValidShiftChangeTimeByTheEndOfTheShift.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelLoadValidShiftChangeTimeByTheEndOfTheShift.Name = "labelLoadValidShiftChangeTimeByTheEndOfTheShift";
            this.labelLoadValidShiftChangeTimeByTheEndOfTheShift.Size = new System.Drawing.Size(896, 32);
            this.labelLoadValidShiftChangeTimeByTheEndOfTheShift.TabIndex = 12;
            this.labelLoadValidShiftChangeTimeByTheEndOfTheShift.Text = "Допустимая длительность пересменки при окончании смены, мин";
            // 
            // labelValidLoadingTime
            // 
            this.labelValidLoadingTime.AutoSize = true;
            this.labelValidLoadingTime.Location = new System.Drawing.Point(353, 226);
            this.labelValidLoadingTime.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelValidLoadingTime.Name = "labelValidLoadingTime";
            this.labelValidLoadingTime.Size = new System.Drawing.Size(702, 32);
            this.labelValidLoadingTime.TabIndex = 13;
            this.labelValidLoadingTime.Text = "Допустимая длительность ожидания погрузки, мин ";
            // 
            // labelValidNumberOfStops
            // 
            this.labelValidNumberOfStops.AutoSize = true;
            this.labelValidNumberOfStops.Location = new System.Drawing.Point(353, 278);
            this.labelValidNumberOfStops.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.labelValidNumberOfStops.Name = "labelValidNumberOfStops";
            this.labelValidNumberOfStops.Size = new System.Drawing.Size(479, 32);
            this.labelValidNumberOfStops.TabIndex = 15;
            this.labelValidNumberOfStops.Text = "Допустимое количество остановок";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(353, 382);
            this.label8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 32);
            this.label8.TabIndex = 16;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonSave);
            this.panel1.Controls.Add(this.numericUpDownTimePeriod);
            this.panel1.Controls.Add(this.numericUpDownValidStopTime);
            this.panel1.Controls.Add(this.numericUpDownValidShiftChangeTimeByTheStartOfTheShift);
            this.panel1.Controls.Add(this.numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift);
            this.panel1.Controls.Add(this.numericUpDownValidLoadingTime);
            this.panel1.Controls.Add(this.numericUpDownValidNumberOfStops);
            this.panel1.Controls.Add(this.labelValidStopTime);
            this.panel1.Controls.Add(this.labelTimePeriod);
            this.panel1.Controls.Add(this.labelValidShiftChangeTimeByTheStartOfTheShift);
            this.panel1.Controls.Add(this.labelLoadValidShiftChangeTimeByTheEndOfTheShift);
            this.panel1.Controls.Add(this.labelValidLoadingTime);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.labelValidNumberOfStops);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1413, 448);
            this.panel1.TabIndex = 17;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1413, 448);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "Form2";
            this.Text = "Настройки";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimePeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValidStopTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValidShiftChangeTimeByTheStartOfTheShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValidLoadingTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownValidNumberOfStops)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTimePeriod;
        private System.Windows.Forms.NumericUpDown numericUpDownTimePeriod;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.NumericUpDown numericUpDownValidStopTime;
        private System.Windows.Forms.NumericUpDown numericUpDownValidShiftChangeTimeByTheStartOfTheShift;
        private System.Windows.Forms.NumericUpDown numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift;
        private System.Windows.Forms.NumericUpDown numericUpDownValidLoadingTime;
        private System.Windows.Forms.NumericUpDown numericUpDownValidNumberOfStops;
        private System.Windows.Forms.Label labelValidStopTime;
        private System.Windows.Forms.Label labelValidShiftChangeTimeByTheStartOfTheShift;
        private System.Windows.Forms.Label labelLoadValidShiftChangeTimeByTheEndOfTheShift;
        private System.Windows.Forms.Label labelValidLoadingTime;
        private System.Windows.Forms.Label labelValidNumberOfStops;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
    }
}