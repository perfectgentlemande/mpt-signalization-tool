﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Signal
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();

            //Вывод дефолтных параметров
            numericUpDownTimePeriod.Value = Settings.LoadTimePeriod()/1000;
            numericUpDownValidStopTime.Value = Settings.LoadValidStopTime() / 60;
            numericUpDownValidShiftChangeTimeByTheStartOfTheShift.Value = Settings.LoadValidShiftChangeTimeByTheStartOfTheShift() / 60;
            numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift.Value = Settings.LoadValidShiftChangeTimeByTheEndOfTheShift() / 60;
            numericUpDownValidLoadingTime.Value = Settings.LoadValidLoadingTime() / 60;
            numericUpDownValidNumberOfStops.Value = Settings.LoadValidNumberOfStops();

            //Запрет на ввод с клавиатуры
            numericUpDownTimePeriod.ReadOnly = true;
            numericUpDownValidStopTime.ReadOnly = true;
            numericUpDownValidShiftChangeTimeByTheStartOfTheShift.ReadOnly = true;
            numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift.ReadOnly = true;
            numericUpDownValidLoadingTime.ReadOnly = true;
            numericUpDownValidNumberOfStops.ReadOnly = true;
        }

        public int TimePeriod //частота обновления (сек)
        {
            get
            {
                //перевод в миллисекунд
                return (int)numericUpDownTimePeriod.Value*1000;
            }
        }
        public int ValidStopTime //Допутимое время остановки(мин)
        {
            get
            {
                //перевод в секунды
                return ((int)numericUpDownValidStopTime.Value) * 60;
            }
        }
        public int ValidShiftChangeTimeByTheStartOfTheShift //Допустимая длительность пересменки при начале смены (мин)
        {
            get
            {
                //перевод в секунды
                return ((int)numericUpDownValidShiftChangeTimeByTheStartOfTheShift.Value) * 60;
            }
        }
        public int ValidShiftChangeTimeByTheEndOfTheShift //Допустимая длительность пересменки при окончании смены (мин)
        {
            get
            {
                //перевод в секунды
                return ((int)numericUpDownLoadValidShiftChangeTimeByTheEndOfTheShift.Value) * 60;
            }
        }
        public int ValidLoadingTime //Допустимая длительность ожидания погрузки (мин)
        {
            get
            {
                //перевод в секунды
                return ((int)numericUpDownValidLoadingTime.Value) * 60;
            }
        }
        public int ValidNumberOfStops //Допустимое количество остановок
        {
            get
            {
                return ((int)numericUpDownValidNumberOfStops.Value);
            }
        }
    }
}
