﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

//Вся штука для белазов. Для пересменки 8 или 20 часов. От этого отсчитывать первую погрузку
//Последняя активная разгурзки до пересменки/начала следующей смены
//Вся штука для белазов. Для пересменки 8 или 20 часов. От этого отсчитывать первую погрузку

namespace Signal
{
    public partial class Form1 : Form
    {
        
        TransactionScript newTransactionScript; //Сценарий транзакций

        //Параметры, вводимые диспетчером и получаемые из Form2
        private int timePeriod; //Частота обновления (миллисекунды)
        private int validStopTime; //Допутимое время остановки (сек)
        private int validShiftChangeTimeByTheStartOfTheShift; //Допустимая длительность пересменки при начале смены (сек)
        private int validShiftChangeTimeByTheEndOfTheShift; //Допустимая длительность пересменки при окончании смены (сек)
        private int validLoadingTime; //Допустимая длительность ожидания погрузки (мин)
        private int validNumberOfStops; //Допустимое количество остановок

        //Параметры, для работы с данными
        private List<int> numbersOfGreenLines = new List<int>(); //Для хранения номеров окрашиваемых зеленым цветом столбцов
        private List<int> numbersOfRedLines; //Для хранения номеров окрашиваемых красным цветом столбцов

        public Form1()
        {
            InitializeComponent();
        }

        private void InitializeForm() //Инициализатор формы
        {

            //Параметры окна формы
            this.Size = new Size(746, 298);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            //Чтение из файла
            timePeriod = Settings.LoadTimePeriod();
            
            //Интервал таймера в миллисекундах
            timer1.Interval = timePeriod;

            //Инициализация сценария транзакций
            validStopTime = Settings.LoadValidStopTime();
            newTransactionScript = new TransactionScript(validStopTime);

            //Инициализация таймера
            this.timer1.Start();

            //Инициализация второго таймера
            timer2 = new Timer() { Interval = 1000 };
            timer2.Tick += timer2_Tick;
            timer2.Start();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            WindowState = FormWindowState.Normal;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //очистка лишних столбцов
            dataGridView1.Columns.Clear();

            //Текущее время в Unix-формате
            int currentUnixTime = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds; 

            //Запуск сценария транзакций
            var result = (from TransactionScriptResult in newTransactionScript.Run(currentUnixTime)
                          select new { DumperStateID = TransactionScriptResult.DumperStateID, DumperID = TransactionScriptResult.DumperID, StartTime = Auxiliary.UnixTimeToDateTime(TransactionScriptResult.StartTime), CurrentState = TransactionScriptResult.CurrentState, Kilometrage = TransactionScriptResult.Kilometrage, StateText = TransactionScriptResult.StateText }).ToList();
            dataGridView1.DataSource = result;
            numbersOfRedLines = newTransactionScript.getNumbersOfUnchangedLines();


            //Проверка на наличие изменившихся состояний, когда они были в подтвержденных, но при этом изменились
            removeAllChangedRowsFromConfirmed();

            //Добавление столбца для кнопок подтверждающих ознакомление
            dataGridView1.Columns.Add("acquitanceStatus", "статус");

            //Удаление из списка красных строк тех, что уже были подтверждены и их окраска
            removeAllConfirmedRedRows();
            paintGreenLines();

            //Окрашивашивание в красный оставшихся строк
            paintRedLines();

            //Количество простоев
            toolStripStatusLabel2.Text = "Количество неподтвержденных простоев: "+ Convert.ToString(numbersOfRedLines.Count);
            toolStripStatusLabel3.Text = "Количество подтвержденных простоев: " + Convert.ToString(numbersOfGreenLines.Count);
            
            if (allLinesAreNotRed())
            {
                //Неподтвержденных простоев нет
                
                showAllAreConfirmedMessage();
            }
            else
            {
                //Есть неизменившиеся состояния
                showUnchangedStatesMessage();
            }
        }
        private void timer2_Tick(object sender, EventArgs e) //Таймер текущего времени
        {
            //Вывод текущей даты и времени
            toolStripStatusLabel1.Text = DateTime.Now.ToLongDateString() + " "+ DateTime.Now.ToLongTimeString();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e) //Нажатие в меню кнопки Настройки для изменения параметров
        {
            //Загрузка формы с параметрами
            loadParametersForm();
        }
        private void toolStripMenuItemHelp_Click(object sender, EventArgs e) //Нажатие в меню кнопки Помощь
        {
            //Загрузка формы с текстом помощи
            loadHelpForm();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //Проверка соединения с БД
            if (Settings.CheckDatabaseConnection())
            {
                InitializeForm();
            }
            else
            {
                MessageBox.Show("Невозможно подключиться к базе данных.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void ToolStripMenuItemParameters_Click(object sender, EventArgs e) //Нажатие кнопки Настройки для изменения параметров из трея
        {
            //Загрузка формы с параметрами из трея
            loadParametersForm();
        }
        private void ToolStripMenuItemClose_Click(object sender, EventArgs e) //Нажатие кнопки Закрыть из трея
        {
            //Закрытие формы из трея
            this.Close();
        }
        private void loadParametersForm() //Загрузка формы с параметрами
        {
            Form2 parametersForm = new Form2();
            parametersForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            parametersForm.MaximizeBox = false;

            //Для отладки в высоком разрешении
            parametersForm.Size = new Size(578, 210);

            if (parametersForm.ShowDialog() == DialogResult.OK)
            {
                //получение параметров из формы
                timePeriod = parametersForm.TimePeriod;
                validNumberOfStops = parametersForm.ValidNumberOfStops;
                validShiftChangeTimeByTheStartOfTheShift = parametersForm.ValidShiftChangeTimeByTheStartOfTheShift;
                validShiftChangeTimeByTheEndOfTheShift = parametersForm.ValidShiftChangeTimeByTheEndOfTheShift;
                validLoadingTime = parametersForm.ValidLoadingTime;
                validStopTime = parametersForm.ValidStopTime;

                //сохранение параметров в файл
                Settings.SaveParameters(timePeriod, validStopTime, validShiftChangeTimeByTheStartOfTheShift, validShiftChangeTimeByTheEndOfTheShift, validLoadingTime, validNumberOfStops);
            }
        }
        private void loadHelpForm() //Загрузка формы с помощью
        {
            Form3 helpForm = new Form3();
            helpForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            helpForm.MaximizeBox = false;

            //Для отладки в высоком разрешении
            helpForm.Size = new Size(578, 210);

            helpForm.Show();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) //Обработчик события при нажатии на кнопку в таблице
        {
            if (e.ColumnIndex == dataGridView1.Columns["acquitanceStatus"].Index && e.RowIndex >= 0)
            {
                //dataGridView1.Columns.Remove(dataGridView1.Columns["acquitanceStatus"]);
                
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Beige;
                cellRepaint(e.RowIndex);
                
            }
        }
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e) //Переключение вкладки
        {
            //При выборе вкладки 2 выводится архив простоев
            if (tabControl1.SelectedTab.Name == "tabPage2")
            {
                dataGridView2.DataSource = DownTimeStopGateway.Read();
            }
        }
        private void saveReportToolStripMenuItem_Click(object sender, EventArgs e) //Нажатие кнопки сохранения отчета о простоях
        {
            saveReport();
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e) //Нажатие из трея кнопки сохранения отчета о простоях
        {
            saveReport();
        }
        private bool allLinesAreNotRed() //проверка, не осталось ли красных строк
        {
            for (int i=0; i<dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].DefaultCellStyle.BackColor == Color.Pink)
                {
                    return false;
                }
            }
            return true;
        }
        private void paintRedLines() //окраска в красный
        {
            for (int i = 0; i < numbersOfRedLines.Count && numbersOfRedLines.Count > 0; i++)
            {
                dataGridView1.Rows[numbersOfRedLines[i]].DefaultCellStyle.BackColor = Color.Pink;
                dataGridView1.Rows[numbersOfRedLines[i]].Cells["acquitanceStatus"] = new DataGridViewButtonCell();
                dataGridView1.Rows[numbersOfRedLines[i]].Cells["acquitanceStatus"].Value = "Подтверждение";
            }
        }
        private void paintGreenLines() //окраска в зеленый
        {
            for (int i = 0; i < numbersOfGreenLines.Count && numbersOfGreenLines.Count > 0; i++)
            {
                dataGridView1.Rows[numbersOfGreenLines[i]].DefaultCellStyle.BackColor = Color.LightGreen;
                dataGridView1.Rows[numbersOfGreenLines[i]].Cells["acquitanceStatus"].Value = "Подтверждено";
                dataGridView1.Rows[numbersOfGreenLines[i]].Cells["acquitanceStatus"].ReadOnly = true;
            }
        }
        private void removeAllConfirmedRedRows() //удаление подтвержденных строк из списка красных
        {
            for (int j = 0; j < numbersOfGreenLines.Count && numbersOfRedLines.Count > 0; j++)
            {
                for (int i = 0; i < numbersOfRedLines.Count; i++)
                {
                    if (numbersOfRedLines[i] == numbersOfGreenLines[j])
                    {
                        numbersOfRedLines.Remove(numbersOfGreenLines[j]);
                    }
                }  
            }
        }
        private void removeAllChangedRowsFromConfirmed() //Удаление измененных состояний из подтвержденных
        {
            if (numbersOfGreenLines.Count!=0)
            {
                for (int i = 0; i < newTransactionScript.getNumbersOfChangedLines().Count; i++)
                {
                    if (numbersOfGreenLines.Exists(item => item == newTransactionScript.getNumbersOfChangedLines()[i]))
                    {
                        numbersOfGreenLines.RemoveAll(item => item == newTransactionScript.getNumbersOfChangedLines()[i]);
                    }
                }
            }
        }
        private void showAllAreConfirmedMessage() //Вывод сообщения о том, что простои подтверждены
        {
            dataGridView1.Columns["acquitanceStatus"].ReadOnly = true;
            notifyIcon1.BalloonTipText = "Неподтвержденные простои отсутствуют";
            notifyIcon1.ShowBalloonTip(1);
        }
        private void showUnchangedStatesMessage() //Вывод сообщения о неизменившихся состояниях
        {
            this.timer1.Stop();
            string additionalNote = "\n\nОбращаем Ваше внимание на то, что повторное обновление приостановлено до тех пор, пока не будет подтверждено ознакомление со статусами всех объектов.\n";
            notifyIcon1.BalloonTipText = "ВНИМАНИЕ! " + Convert.ToString(numbersOfRedLines.Count) + " объектов не изменили свой статус." + additionalNote;
            notifyIcon1.ShowBalloonTip(1);
            MessageBox.Show("ВНИМАНИЕ! " + Convert.ToString(numbersOfRedLines.Count) + " объектов не изменили свой статус. Нажмите ОК для ознакомления." + additionalNote, "ОПАСНОСТЬ", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void cellRepaint(int cellNo) //Перекраска нужной ячейки
        {
            if (!numbersOfGreenLines.Contains(cellNo))
            {
                numbersOfGreenLines.Add(cellNo);
            }
            if (numbersOfRedLines.Contains(cellNo))
            {
                numbersOfRedLines.Remove(cellNo);
            }
            
            //Очистка столбца
            dataGridView1.Columns.Remove(dataGridView1.Columns["acquitanceStatus"]);

            //Очистка столбца
            dataGridView1.Columns.Add("acquitanceStatus", "статус");

            //Повторная окраска
            paintRedLines();
            paintGreenLines();

            if (allLinesAreNotRed())
            {
                dataGridView1.Columns["acquitanceStatus"].ReadOnly = true;
                timer1.Start();
            }
        }
        private void saveReport() //Сохранение отчета в csv
        {
            dataGridView2.DataSource = DownTimeStopGateway.Read();
            string csv = string.Empty;


            //Добавление заголовка
            foreach (DataGridViewColumn column in dataGridView2.Columns)
            {
                csv += column.HeaderText + ';';
            }

            //Добавление пустой строки
            csv += "\r\n";

            //Adding the Rows
            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    //Добавление данных
                    csv += cell.Value.ToString().Replace(";", ",") + ';';
                }

                //Добавление пустой строки
                csv += "\r\n";
            }

            //Запись в файл
            Settings.SaveCSVReport("Отчет о простоях", csv);
        }
    }
}
