﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Signal
{
    //Шлюз таблицы данных DownTimeStop
    public static class DownTimeStopGateway
    {
        public static List<DownTimeStop> Read()
        {
            var result = new List<DownTimeStop>(); //список для хранения результата выборки

            //соединение с БД
            SqlConnection connectionDownTimeStop = new SqlConnection(Settings.LoadConnectionString());
            connectionDownTimeStop.Open();

            //отправка запроса
            SqlCommand cmd = new SqlCommand("SELECT TOP 5 [dbo].[DownTimeStop].[DownTimeID],[dbo].[DownTimeStop].[DumperStateID],[dbo].[DownTimeType].[DownTimeTypeName],[dbo].[DownTimeStop].[AdditionalInfo] FROM [dbo].[DownTimeStop] JOIN [dbo].[DownTimeType] ON [dbo].[DownTimeType].[DownTimeTypeID]=[dbo].[DownTimeStop].[DownTimeTypeID]", connectionDownTimeStop);
            SqlDataReader reader = cmd.ExecuteReader();

            //считывание значений
            while (reader.Read())
            {
                result.Add(new DownTimeStop() { DownTimeID = reader.GetInt32(0), DumperStateID = reader.GetInt32(1), DownTimeTypeName = reader.GetString(2), AdditionalInfo = reader.GetString(3) });
            }
            connectionDownTimeStop.Close();
            return result;
        }
    }
    public class DownTimeStop
    {
        public int DownTimeID { get; set; }
        public int DumperStateID { get; set; }
        public string DownTimeTypeName { get; set; }
        public string AdditionalInfo { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", DownTimeID, DumperStateID, DownTimeTypeName, AdditionalInfo);
        }
    }
}
