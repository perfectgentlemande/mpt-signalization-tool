﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Signal
{
    //Шлюз таблицы данных DumperState
    public static class DumperStateGateway
    {
        public static List<DumperState> Read()
        {
            var result = new List<DumperState>(); //список для хранения результата выборки
            
            //соединение с БД
            SqlConnection connectionDumperState = new SqlConnection(Settings.LoadConnectionString());
            connectionDumperState.Open();
            
            //отправка запроса
            SqlCommand cmd = new SqlCommand("SELECT TOP 5 [DumperStateID],[DumperID],[StartTime],[CurrentState],[Kilometrage],[StateText] FROM [dbo].[DumperState]", connectionDumperState);
            SqlDataReader reader = cmd.ExecuteReader();
            
            //считывание значений
            while (reader.Read())
            {
                result.Add(new DumperState() { DumperStateID = reader.GetInt32(0), DumperID = reader.GetInt32(1), StartTime = reader.GetInt32(2), CurrentState = reader.GetInt32(3), Kilometrage = reader.GetInt32(4), StateText =reader.GetString(5)});
            }
            connectionDumperState.Close();
            return result;
        }
        public static DumperState find(int id)
        {
            DumperState result = new DumperState();

            //соединение с БД
            SqlConnection connectionDumperState = new SqlConnection(Settings.LoadConnectionString());
            connectionDumperState.Open();

            //отправка запроса
            SqlCommand cmd = new SqlCommand("SELECT [DumperStateID],[DumperID],[StartTime],[CurrentState],[Kilometrage],[StateText] FROM [dbo].[DumperState] WHERE [DumperStateID]="+Convert.ToString(id)+";", connectionDumperState);
            SqlDataReader reader = cmd.ExecuteReader();

            //считывание значений
            while (reader.Read())
            {
                result=new DumperState() { DumperStateID = reader.GetInt32(0), DumperID = reader.GetInt32(1), StartTime = reader.GetInt32(2), CurrentState = reader.GetInt32(3), Kilometrage = reader.GetInt32(4), StateText = reader.GetString(5) };
            }
            connectionDumperState.Close();
            return result;
        }
    }
    public class DumperState
    {
        public int DumperStateID { get; set; }
        public int DumperID { get; set; }
        public int StartTime { get; set; }
        public int CurrentState { get; set; }
        public int Kilometrage { get; set; }
        public string StateText { get; set; }
        
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5}", DumperStateID, DumperID, StartTime, CurrentState, Kilometrage, StateText);
        }
    }
}
