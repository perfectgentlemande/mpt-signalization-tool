﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal
{
    /// <summary>
    /// Содержит различные вспомогательные функции
    /// </summary>
    class Auxiliary
    {
        /// <summary>
        /// Конвертирует время в формате UnixTime в формат DateTime
        /// </summary>
        /// <param name="unixTime">время в формате UnixTime</param>
        /// <returns></returns>
        public static DateTime UnixTimeToDateTime(int unixTime)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTime).ToLocalTime();
            return dateTime;
        }

        /// <summary>
        /// Конвертирует время в формате DateTime в формат UnixTime
        /// </summary>
        /// <param name="dateTime">время в формате DateTime</param>
        /// <returns></returns>
        public static int DateTimeToUnixTime(DateTime dateTime)
        {
            int unixTime = (int)(dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalSeconds;
            return unixTime;
        }
    }
}
