﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Signal
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            labelHelp.Text += "\n\n";
            labelHelp.Text += "Отсчет не изменившихся состояний ведется относительно текущего периода обновления.\n";
            labelHelp.Text += "Введенные в разделе Настройки параметры вступают в силу только после перезагрузки программы.\n";
        }
    }
}
