﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Signal
{
    //сценарий транзакций для отслеживания изменениц состояний
    class TransactionScript
    {
        private List<int> numbersOfUnchangedLines; //для хранения номеров неизменившихся состояний
        private List<int> numbersOfChangedLines; //для хранения номеров изменившихся состояний

        private List<DumperState> previousStepDumperStates; //для хранения состояний на предыдущем шаге
        private List<DumperState> currentStepDumperStates; //для хранения состояний на текущем шаге

        private int timePeriod; //время шага

        public TransactionScript (int newTimePeriod) //инициализация сценария транзакций
        {
            previousStepDumperStates = DumperStateGateway.Read();
            timePeriod = newTimePeriod;
        }

        public List<DumperState> Run(int currentUnixTime) //сценарий транзакций
        {
            numbersOfUnchangedLines = new List<int>();
            numbersOfChangedLines = new List<int>();
            currentStepDumperStates = DumperStateGateway.Read();

            //выборка не изменившихся за шаг состояний
            var unchangedAfterTheStepDumperStates = (from psds in previousStepDumperStates
                                         join csds in currentStepDumperStates
                                         on psds.DumperStateID equals csds.DumperStateID
                                         where psds.CurrentState==csds.CurrentState
                                         select new DumperState { DumperStateID=csds.DumperStateID, DumperID=csds.DumperID, StartTime=csds.StartTime, CurrentState=csds.CurrentState, Kilometrage=csds.Kilometrage, StateText=csds.StateText }).ToList();

            //выборка из неизменившихся за время обновления 
            //для теста оставлено без времени остановки
            unchangedAfterTheStepDumperStates = (from uatsds in unchangedAfterTheStepDumperStates
                                                 where (currentUnixTime - uatsds.StartTime) > timePeriod
                                                 select new DumperState { DumperStateID = uatsds.DumperStateID, DumperID = uatsds.DumperID, StartTime = uatsds.StartTime, CurrentState = uatsds.CurrentState, Kilometrage = uatsds.Kilometrage, StateText = uatsds.StateText }).ToList();


            //определение номеров изменившихся и неизменившихся строк
            for (int i = 0; i< currentStepDumperStates.Count; i++)
            {
                for (int j = 0; j < unchangedAfterTheStepDumperStates.Count; j++)
                {
                    if (currentStepDumperStates[i].DumperStateID == unchangedAfterTheStepDumperStates[j].DumperStateID)
                    {
                        numbersOfUnchangedLines.Add(i);
                    }
                }
            }

            //Для удобства нахождения номеров измененных строк
            List<int> numbersOfAllLines = new List<int>();
            for (int i=0; i < currentStepDumperStates.Count; i++)
            {
                numbersOfAllLines.Add(i);
            }

            //Номера измененных строк
            numbersOfChangedLines = (from noal in numbersOfAllLines
                                    select noal).Except(from noul in numbersOfUnchangedLines
                                                        select noul).ToList(); 

            previousStepDumperStates = currentStepDumperStates;
            return currentStepDumperStates;
        }
        public List<int> getNumbersOfUnchangedLines() //номера неизмененных строк
        {
            return numbersOfUnchangedLines;
        }
        public List<int> getNumbersOfChangedLines() //номера измененных строк
        {
            return numbersOfChangedLines;
        }
    }
}
