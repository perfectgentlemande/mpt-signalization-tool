USE [TrackKGOK]
GO

/****** Object:  Table [dbo].[DownTimeStop]    Script Date: 04.02.2018 2:30:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DownTimeStop](
	[DownTimeID] [int] IDENTITY(1,1) NOT NULL,
	[DumperStateID] [int] NOT NULL,
	[DownTimeTypeID] [int] NOT NULL,
	[AdditionalInfo] [nvarchar](60) NULL,
 CONSTRAINT [PK_DownTimeStop] PRIMARY KEY CLUSTERED 
(
	[DownTimeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DownTimeStop]  WITH CHECK ADD  CONSTRAINT [FK_DownTimeStop_DownTimeType] FOREIGN KEY([DownTimeTypeID])
REFERENCES [dbo].[DownTimeType] ([DownTimeTypeID])
GO

ALTER TABLE [dbo].[DownTimeStop] CHECK CONSTRAINT [FK_DownTimeStop_DownTimeType]
GO

ALTER TABLE [dbo].[DownTimeStop]  WITH CHECK ADD  CONSTRAINT [FK_DownTimeStop_DumperState] FOREIGN KEY([DumperStateID])
REFERENCES [dbo].[DumperState] ([DumperStateID])
GO

ALTER TABLE [dbo].[DownTimeStop] CHECK CONSTRAINT [FK_DownTimeStop_DumperState]
GO


